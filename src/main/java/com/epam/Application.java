package com.epam;

import view.*;
import java.io.IOException;
/**
 * This class is the enter point in  com.epam.Application.
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class Application {
    /**
     * It is the main method of com.epam.Application.
     *
     * @param  args in method main
     */
    public static void main(String[] args) {
        try {
            new View().show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
