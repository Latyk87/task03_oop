package view;

import controller.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.BufferedReader;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));
    private static int fuelRange;

    public View() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Max tonnage of whole company planes");
        menu.put("2", "  2 - Max passengers capacity of whole company planes");
        menu.put("3", "  3 - Find the plane by fuel parameter");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);

    }

    private void pressButton1() {
        System.out.println(controller.getcalculateTonnage());
    }

    private void pressButton2() {
        System.out.println(controller.getcalculateCapacity());
    }

    private void pressButton3() {
        System.out.println("Input the fuel parameter from 0 to 8000 L-h");
        BufferedReader reader = new BufferedReader
                (new InputStreamReader(System.in));
        try {
            fuelRange = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(controller.findbyFuel(fuelRange));
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nEPAM AIRLINES:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws IOException {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();

            } catch (Exception e) {
                System.out.println("Wrong data, please type again");
            }
        } while (!keyMenu.equals("Q"));
    }
}
