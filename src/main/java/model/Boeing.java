package model;

/**
 * Class describes the Boeing Planes.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class Boeing extends Airoplane {
    public Boeing(String model, int tonnage, int capacity,
                  int distanceofFlight, int fuel) {
        super(model, tonnage, capacity, distanceofFlight, fuel);
    }
}
