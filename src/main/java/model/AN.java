package model;

/**
 * Class describes the AN Planes.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class AN extends Airoplane {
    public AN(String model, int tonnage, int capacity,
              int distanceofFlight, int fuel) {

        super(model, tonnage, capacity, distanceofFlight, fuel);
    }
}
