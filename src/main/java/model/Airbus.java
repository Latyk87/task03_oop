package model;

/**
 * Class describes the Airbus Planes.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class Airbus extends Airoplane {
    public Airbus(String model, int tonnage, int capacity,
                  int distanceofFlight, int fuel) {
        super(model, tonnage, capacity, distanceofFlight, fuel);
    }
}
