package model;


import java.util.TreeSet;

/**
 * Class contains logic methods to make engine of program workable.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class BusinessLogic implements Model {

    private TreeSet<Airoplane> planes;
    private TreeSet<Airoplane> availableplanes;

    public BusinessLogic() {
        planes = new TreeSet<>();
        planes.add(plane1);
        planes.add(plane2);
        planes.add(plane3);
        planes.add(plane4);
        planes.add(plane5);
        planes.add(plane6);
        planes.add(plane7);
        planes.add(plane8);
        planes.add(plane9);
        planes.add(plane10);
    }

    Airoplane plane6 = new Boeing("Boeing-747", 450, 380, 13000, 8000);
    Airoplane plane1 = new Boeing("Boeing-737", 400, 300, 10000, 6500);

    Airoplane plane7 = new Airbus("A-280", 290, 230, 7500, 3800);
    Airoplane plane2 = new Airbus("A-320", 300, 250, 8000, 4200);
    Airoplane plane10 = new Airbus("A-410", 330, 270, 9000, 5500);

    Airoplane plane3 = new Tupolev("TU-170", 250, 180, 5500, 3000);
    Airoplane plane8 = new Tupolev("TU-154", 280, 200, 6000, 3500);

    Airoplane plane4 = new AN("AN-140", 200, 180, 4000, 2000);
    Airoplane plane5 = new AN("AN-158", 220, 200, 4800, 2400);
    Airoplane plane9 = new AN("AN-180", 235, 220, 5200, 2800);

    @Override
    public int getcalculateTonnage() {
        int wholetonnage = 0;
        for (Airoplane t : planes) {
            wholetonnage += t.getTonnage();
        }
        return wholetonnage;
    }

    @Override
    public int getcalculateCapacity() {
        int passangeres = 0;
        for (Airoplane t : planes) {
            passangeres += t.getCapacity();
        }
        return  passangeres;
    }

    public TreeSet<Airoplane> getAvailableplanes() {
        return availableplanes;
    }

    @Override
    public TreeSet<Airoplane> getPlanes() {
        return planes;
    }

    @Override
    public TreeSet<Airoplane> findbyFuel(int a) {
        availableplanes = new TreeSet<>();
        for (Airoplane b : planes) {
            if (b instanceof AN && a >= 0 && a <= plane9.getFuel()) {
                availableplanes.add(b);
            } else if (b instanceof Tupolev && a >= plane9.getFuel()
                    && a <= plane8.getFuel()) {
                availableplanes.add(b);
            } else if (b instanceof Airbus && a >= plane8.getFuel()
                    && a <= plane10.getFuel()) {
                availableplanes.add(b);
            } else if (b instanceof Boeing && a >= plane10.getFuel()
                    && a <= plane6.getFuel()) {
                availableplanes.add(b);
            }
        }
        return availableplanes;
    }
}

