package model;

/**
 * Class is the mother class for whole the planes.
 * Class implements interface Comparable
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public abstract class Airoplane implements Comparable<Airoplane> {
    /**
     * Instance variable for planes.
     */
    private String model;
    /**
     * Instance variable for planes.
     */
    private int tonnage;
    /**
     * Instance variable for planes.
     */
    private int capacity;
    /**
     * Instance variable for planes.
     */
    private int distanceofFlight;
    /**
     * Instance variable for planes.
     */
    private int fuel;
    /**
     * Instance variable for planes.
     */

    /**
     * It is the main constructor for whole objects of Planes .
     * Created by Borys Latyk on 13/11/2019.
     */
    public Airoplane(String model, int tonnage, int capacity,
                     int distanceofFlight, int fuel) {
        this.model = model;
        this.tonnage = tonnage;
        this.capacity = capacity;
        this.distanceofFlight = distanceofFlight;
        this.fuel = fuel;
    }

    public int getFuel() {
        return fuel;
    }

    public String getModel() {
        return model;
    }

    public int getTonnage() {
        return tonnage;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getDistanceofFlight() {
        return distanceofFlight;
    }

    public int compareTo(Airoplane o) {
        if (this.distanceofFlight == o.distanceofFlight) {
            return 0;
        } else if (this.distanceofFlight < o.distanceofFlight) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return this.getModel() + " " + this.getDistanceofFlight() + " км";
    }
}