package controller;

import model.Airoplane;
import java.util.TreeSet;

/**
 * Interface for controller.
 * Created by Borys Latyk on 13/11/2019.
 * @version 2.1
 * @since 10.11.2019
 */
public interface Controller {
    int getcalculateTonnage();

    int getcalculateCapacity();

    TreeSet<Airoplane> getPlanes();

    TreeSet<Airoplane> findbyFuel(int a);

    TreeSet<Airoplane> getAvailableplanes();

}