package controller;

import model.Airoplane;
import model.BusinessLogic;
import model.Model;

import java.util.TreeSet;
/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerImpl implements Controller {
    private Model link;

    public ControllerImpl() {
        link = new BusinessLogic();
    }

    @Override
    public int getcalculateTonnage() {
        return link.getcalculateTonnage();
    }

    @Override
    public int getcalculateCapacity() {
        return link.getcalculateCapacity();
    }

    @Override
    public TreeSet<Airoplane> getPlanes() {
        return link.getPlanes();
    }

    @Override
    public TreeSet<Airoplane> findbyFuel(int a) {
        return link.findbyFuel(a);
    }

    @Override
    public TreeSet<Airoplane> getAvailableplanes() {
        return link.getAvailableplanes();
    }


}

